from itertools import permutations
import sys
import random

def overlap(a, b, min_length):
    start = 0

    while True:
        start = a.find(b[:min_length], start)
        if start == -1:
            return 0
        if b.startswith(a[start:]):
            return len(a) - start
        start += 1


def naive_overlap_map(reads, k):
    olaps = {}
    graph = {}
    for i in reads:
        graph[i] = set()
    nodos = set()
    for a,b in permutations(reads, 2):
        olen = overlap(a, b, k)
        if olen > 0:
            olaps[(a,b)] = olen
            graph[a].add( b )
            nodos.add(a)
    return olaps, nodos, graph


def scs(ss):
     shortest_sup  = None

     for ssperm in permutations(ss):
         sup = ssperm[0]
         for i in range(len(ss)-1):
            olen =  overlap(ssperm[i], ssperm[i+1], min_length=1)
            sup += ssperm[i+1][olen:]
         if shortest_sup is None or len(sup) < len(shortest_sup):
            shortest_sup = sup
     return shortest_sup


def disposicion1(graph, nodos):
    for i in nodos:     # GTACGT
        for j in list(graph[i]):  #TACGTA
            for k in list(graph[j]): #ACGTAC
                if k in graph[i]:
                    graph[i].remove(k)


def disposicion2(graph, nodos):
    for i in nodos:     # GTACGT
        for j in list(graph[i]):  #TACGTA
            for k in list(graph[j]): #ACGTAC
                for l in list(graph[k]):
                    if l in graph[i]:
                        graph[i].remove(l)


def union(graph, listNodos, setNodos, r):
    for i in listNodos:
        if len(graph[i]) != 1:
            continue
        u, padre = checkUnicoPadre(i, graph, set(listNodos))
        v, _ = checkUnicoPadre(graph[i][0], graph, set(listNodos))
        if u and v:
            unir(i, graph[i][0], graph, padre, listNodos, setNodos, r)


def checkUnicoPadre(i, graph, nodos):
    count = 0
    padre = -1
    for j in nodos:
        if j == i:
            continue
        if i in graph[j]:
            count += 1
            padre = j
            if count > 1:
                return False, i
    return True, padre


def unir(a, b, graph, padre, listNodos, setNodos, r):

    nuevoNodo = a + b[r[a,b]:]
    setNodos.add(nuevoNodo)
    listNodos.append(nuevoNodo)
    listNodos.remove(a)
    listNodos.remove(b)
    graph[nuevoNodo] = graph[b]

    if padre != -1:
        graph[padre].remove(a)
        graph[padre].append(nuevoNodo)
        r[padre, nuevoNodo] = r[a, b]
        for i in graph[b]:
            r[nuevoNodo, i] = r[b, i]
    graph.pop(a)
    graph.pop(b)


def pick_maximal_overlap(reads,k):
    readA, readB = None, None
    best_olen = 0
    for a,b in permutations(reads, 2):
        olen = overlap(a,b, min_length=k)
        if olen > best_olen:
            readA, readB = a, b
            best_olen = olen
    return readA, readB, best_olen


def greedy_scs(reads, k):
    read_a, read_b, olen = pick_maximal_overlap(reads, k)
    while olen > 0:
        reads.remove(read_a)
        reads.remove(read_b)
        reads.append(read_a + read_b[olen:])
        read_a, read_b, olen = pick_maximal_overlap(reads,k)
    return ''.join(reads)


def readFastq(filename):
    sequences = []
    qualities = []
    with open(filename) as fh:
        while True:
            fh.readline() # skip name line
            seq = fh.readline().rstrip() # read base sequence
            fh.readline() # skip placeholder line
            qual = fh.readline().rstrip() #base quality line
            if len(seq) == 0:
                break
            sequences.append(seq)
            qualities.append(qual)
    return sequences


contigs = []


def buscarPadres(a):
    list = []
    for x in graph:
        if a in graph[x]:
            list.append(x)
    return list


def unirContigs(graph, visited):
    if len(contigs) > 1:

        nuevoNodo = contigs[0]
        visited[nuevoNodo] = True
        graph.pop(nuevoNodo)

        for i in range(len(contigs) - 1):
            b = contigs[i+1]
            nuevoNodo += b[r[contigs[i], b]:]
            ultimosHijos = graph[b]
            graph.pop(b)
            visited[b] = True

        listaPadres = buscarPadres(contigs[0])

        for x in listaPadres:
            graph[x].remove(contigs[0])
            graph[x].add(nuevoNodo)
            r[x, nuevoNodo] = r[x, contigs[0]]

        graph[nuevoNodo] = ultimosHijos
        for x in ultimosHijos:
            r[nuevoNodo, x] = r[contigs[-1], x]
        visited[nuevoNodo] = False

        return  nuevoNodo
    elif len(contigs) == 1:
        return contigs[0]


def DFSUtil(graph, v, visited):

    # Mark the current node as visited and print it
    # Recur for all the vertices adjacent to
    # this vertex
    stack = []
    stack.append(v)

    while (len(stack) != 0):
        v = stack.pop()
        if visited[v] == False:

            # print(v),
            visited[v] = True

            unique, padre = checkUnicoPadre(v, graph, graph.keys())

            if len(contigs) > 0:
                if unique:
                    # print('contigs > 0 && unique')
                    # print(contigs)
                    contigs.append(v)

                    if len(graph[v]) > 1:
                        # print('graph[v]) > 1')
                        # print(contigs)
                        v = unirContigs(graph, visited)
                        contigs.clear()
                    elif len(graph[v]) < 1:
                        # print('graph[v]) < 1')
                        # print(contigs)
                        v = unirContigs(graph, visited)
                        contigs.clear()
                else:
                    # print('contigs > 0 && !unique')
                    # print(contigs)
                    unirContigs(graph, visited)
                    contigs.clear()
            else:
                # print('contigs < 0 ')
                # print(contigs)
                contigs.append(v)

                if len(graph[v]) > 1:
                    # print('graph[v]) > 1')
                    # print(contigs)
                    contigs.clear()
                elif len(graph[v]) < 1:
                    # print('graph[v]) < 1')
                    # print(contigs)
                    contigs.clear()



            for x in graph[v]:
                if visited[x] == False:
                    stack.append(x)
                else:
                    unirContigs(graph, visited)
                    contigs.clear()


def allVisited(visited):
    for x in visited:
        if visited[x] == False:
            return x
    return None


def DFS(graph, f):
    visited = {}
    for i in graph:

    # Mark all the vertices as not visited
        visited[i] = False

    # Call the recursive helper function to print
    # DFS traversal starting from all vertices one
    # by one
    i = allVisited(visited)
    while i:
        if visited[i] == False:
            f(graph, i, visited)
        i = allVisited(visited)

    # print(visited)


def greedy(graph):
    a,b = DFS(graph, biggest)

# reads = ['to_ever', 'o_every', '_every_', 'every_t', 'very_th', 'ery_thi', 'ry_thin', 'y_thing','_thing_']
reads = readFastq(sys.argv[1])
r, nodos, graph = naive_overlap_map(reads,8)

###########################################################################################
def buscarAristaMayorPeso(grafo, r):
    nodos = grafo.keys()
    mayorPeso = 0
    nodoPrefijo = None
    nodoSufijo = None

    for nodo in nodos:
        aristas = None
        aristas = grafo.get(nodo)
        if aristas != None:
            for arista in aristas:
                #print("r:", r)
                #print("r[nodo, arista]:", r[nodo, arista])

                if  r[nodo, arista] > mayorPeso:
                    mayorPeso = r[nodo, arista]
                    nodoPrefijo = nodo
                    nodoSufijo = arista
    if nodoPrefijo != None and nodoSufijo != None:
        return nodoPrefijo, nodoSufijo
    else:
        return None, None


def quitarNodo(grafo, nodo):
    if nodo in grafo:
        grafo.pop(nodo)
    return grafo


def agregarNodo(grafo, nodo, aristas, nodoSufijo, nodoPrefijo):
    if aristas != None:
        aristas = aristas.difference({nodoPrefijo})
        grafo[nodo] = aristas
        for arista in aristas:
            r[nodo, arista] = r[nodoSufijo, arista]

    return grafo
    

def generarArchivo(genomaEnsamblado):
    linea = ""
    lista = []
    i = 0

    while i < len(genomaEnsamblado):
        if i%70 != 0:
            linea += genomaEnsamblado[i]
        elif (i%70 == 0):
            lista.append(linea)
            linea = ""
            linea += genomaEnsamblado[i]
        i += 1
    lista.append(linea)


    resultado = open("resultado.fa", "w")
    resultado.write(">testPhixMod")
    resultado.close()

    resultado = open("resultado.fa", "a")
    for i in range(len(lista)):
        resultado.write(lista[i] + "\n")
    resultado.close()


# def actualizarR(grafo, nuevoNodo, nodoPrefijo, r):
#     nodos = grafo.keys()
#     k = 13
#     for nodo in nodos:
#         aristas = grafo.get(nodo)
#         if nodoPrefijo in aristas:
#             aristas.remove(nodoPrefijo)
#     for nodo in nodos:
#         solapamiento = overlap(nodo, nuevoNodo, 13)
#         if solapamiento > 0:
#             if (nodo, nuevoNodo) in r:
#                 r[nodo, nuevoNodo].append(solapamiento)
#             else:
#                 r[nodo, nuevoNodo] = solapamiento
#     return r


def buscarActualizarPadres(grafo, nodoEliminar, nuevoNodo, nodoSufijo):
    # print('del')
    # print(nodoEliminar,nuevoNodo)
    nodos = grafo.keys()
    for vertice in nodos:
        if vertice == nodoEliminar or vertice == nodoSufijo:
            continue
        for conexion in list(grafo[vertice]):

            if conexion == nodoEliminar:
                # if nodoSufijo == vertice:
                #     grafo[vertice].remove(nodoEliminar)
                #     continue

                grafo[vertice].remove(nodoEliminar)
                grafo[vertice].add(nuevoNodo)

                r[vertice, nuevoNodo] = r[vertice, nodoEliminar]
    return grafo


def buscarEliminarAristasASufijo(grafo, nodoSufijo):
    nodos = grafo.keys()
    for vertice in nodos:

        for conexion in list(grafo[vertice]):

            if conexion == nodoSufijo:
                grafo[vertice].remove(nodoSufijo)
    return grafo

def algoritmoVoraz(grafo, r):
    concatenar = False
    salida = ""

    while concatenar == False:
        nodoPrefijo, nodoSufijo = buscarAristaMayorPeso(grafo, r)
        if nodoPrefijo != None and nodoSufijo != None:
            solapamiento = r[nodoPrefijo, nodoSufijo]
            nuevoNodo = nodoPrefijo + nodoSufijo[solapamiento:]



            grafo = buscarActualizarPadres(grafo, nodoPrefijo, nuevoNodo, nodoSufijo)

            grafo = buscarEliminarAristasASufijo(grafo, nodoSufijo)

            grafo = agregarNodo(grafo, nuevoNodo, grafo.get(nodoSufijo), nodoSufijo, nodoPrefijo)

            grafo = quitarNodo(grafo, nodoPrefijo)
            grafo = quitarNodo(grafo, nodoSufijo)

            # nodos = grafo.keys()
            # r = actualizarR(grafo, nuevoNodo, nodoSufijo, r)
        else:
            concatenar = True

    nodos = grafo.keys()
    if len(nodos) > 1:
        print('>1')
        for nodo in nodos:
            salida += nodo
    elif len(nodos) == 1:
        print('1')
        for nodo in nodos:
            salida += nodo
    generarArchivo(salida)
    print('done')


###########################################################################################


# listNodos = list(nodos)
# print(len(listNodos))
# # print(reads)
# #
disposicion1(graph, nodos)
disposicion2(graph, nodos)
# # print(nodos)
# union(graph, listNodos, nodos, r)
#
DFS(graph, DFSUtil)
algoritmoVoraz(graph, r)


# greedy_scs(graph)
# print(len(listNodos))
# # print(r)
# # print(nodos)
# print(greedy_scs(listNodos,30))


# Python program to print DFS traversal for complete graph
from collections import defaultdict




# This code is contributed by Neelam Yadav


